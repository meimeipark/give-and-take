package com.pym.giveandtakeapi.service;

import com.pym.giveandtakeapi.entity.Friend;
import com.pym.giveandtakeapi.model.friend.*;
import com.pym.giveandtakeapi.repository.FriendRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FriendService {
    private final FriendRepository friendRepository;

    public Friend getData(long id){
        return friendRepository.findById(id).orElseThrow();
    }

    public void setFriend (FriendCreateRequest request) {
        Friend addData = new Friend();
        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setEtcMemo(request.getEtcMemo());

        addData.setCutType(false); //최초 등록은 false 값으로 지정

        friendRepository.save(addData);
    }

    public List<FriendItem> getFriends(){
        List<Friend> originList = friendRepository.findAll();

        List<FriendItem> result = new LinkedList<>();

        for (Friend friend: originList){
            FriendItem addItem = new FriendItem();
            addItem.setId(friend.getId());
            addItem.setName(friend.getName());
            addItem.setPhoneNumber(friend.getPhoneNumber());

            result.add(addItem);
        }
        return result;
    }

    public FriendResponse getFriend(long id){
        Friend originData = friendRepository.findById(id).orElseThrow();

        FriendResponse response = new FriendResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setEtcMemo(originData.getEtcMemo());
        response.setCutType(originData.getCutType() ? "손절":"친구");
        response.setCutDate(originData.getCutDate());
        response.setCutReason(originData.getCutReason());

        return response;
    }

    public void putFriendCut(long id, FriendCutChangeRequest request){
        Friend originData = friendRepository.findById(id).orElseThrow();

        originData.setCutType(true);
        originData.setCutReason(request.getCutReason());
        originData.setCutDate(LocalDate.now());

        friendRepository.save(originData);
    }

    public void putFriendInfo(long id, FriendInfoChangeRequest request){
        Friend originData = friendRepository.findById(id).orElseThrow();

        originData.setName(request.getName());
        originData.setPhoneNumber(request.getPhoneNumber());
        originData.setEtcMemo(request.getEtcMemo());

        friendRepository.save(originData);
    }

    public void putFriendCutCancel(long id){
        Friend originData = friendRepository.findById(id).orElseThrow();

        originData.setCutType(false);
        originData.setCutDate(null);
        originData.setCutReason(null);

        friendRepository.save(originData);
    }
}
