package com.pym.giveandtakeapi.service;

import com.pym.giveandtakeapi.entity.Member;
import com.pym.giveandtakeapi.model.member.MemberDupCheckResponse;
import com.pym.giveandtakeapi.model.member.MemberJoinRequest;
import com.pym.giveandtakeapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public MemberDupCheckResponse getMemberIdDupCheck(String username){
        MemberDupCheckResponse response = new MemberDupCheckResponse();
        response.setIsNew(isNewUsername(username));

        return response;
    }

    /**
     * 회원을 가입 시킨다.
     *
     * @param request 회원 가입 창에서 고객이 작성한 정보
     * @throws Exception 아이디 중복일 경우, 비밀번호와 비밀번호 확인이 일치 하지 않을 경우
     */


    //회원 가입
    //Exception 최상위 예외
    public void setMemberJoin (MemberJoinRequest request) throws Exception {
        if (!isNewUsername(request.getUsername())) throw new Exception();
        if (!request.getPassword().equals(request.getPasswordRe())) throw  new Exception();
        Member addData = new Member();
        addData.setUsername(request.getUsername());
        addData.setPassword(request.getPassword());
        addData.setName(request.getName());
        addData.setDateJoin(LocalDateTime.now());

        memberRepository.save(addData);
    }
    /**
     * 신규 아이디인지 확인한다.
     * @param username 아이디
     * @return true 신규아이디 / false 중복 아이디
     */
    // id 중복확인을 참거짓으로 표현해라(id를 받아)
    // 중복검사를 했을 때 0보다 크면 돌려줘라

    //코드 리팩토링(효율적으로 사용하기 위해)
    // 아이디 중복 확인 api 가 결과 타입이 boolean이라서 프론트가 받을 때 true 나 false로 받게된다.
    // 즉, json으로 받는게 아니다. 그래서 프론트에서 이 api 이용이 불가하여 해결하기 위해서는 Response로 담아준다.
    // 이걸 해결하기 위해 리팩토링 진행.
    private boolean isNewUsername (String username){
        long dupCount = memberRepository.countByUserName(username);

        // dupCount > 0 : true, dupCount = 0 : false
        return dupCount <= 0;
    }

}
