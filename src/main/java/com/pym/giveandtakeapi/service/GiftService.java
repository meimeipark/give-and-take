package com.pym.giveandtakeapi.service;

import com.pym.giveandtakeapi.entity.Friend;
import com.pym.giveandtakeapi.entity.Gift;
import com.pym.giveandtakeapi.model.gift.*;
import com.pym.giveandtakeapi.repository.GiftRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GiftService {
    private final GiftRepository giftRepository;

    public void setGift (Friend friend, GiftCreateRequest request){
        Gift addData = new Gift();
        addData.setFriend(friend);
        addData.setGiftType(request.getGiftType());
        addData.setContent(request.getContent());
        addData.setPrice(request.getPrice());
        addData.setGiftDay(request.getGiftDay());

        giftRepository.save(addData);
    }

    public List<GiftItem> getGifts(){
        List<Gift> originList = giftRepository.findAll();

        List<GiftItem> result = new LinkedList<>();

        for(Gift gift: originList){
            GiftItem addItem = new GiftItem();
            addItem.setId(gift.getId());
            addItem.setFriendName(gift.getFriend().getName());
            addItem.setGiftType(gift.getGiftType().getType());
            addItem.setContent(gift.getContent());

            result.add(addItem);
        }
        return result;
    }

    public GiftResponse getGift(long id){
        Gift originData = giftRepository.findById(id).orElseThrow();

        GiftResponse response = new GiftResponse();
        response.setFriendId(originData.getFriend().getId());
        response.setFriendName(originData.getFriend().getName());
        response.setFriendPhoneNumber(originData.getFriend().getPhoneNumber());
        response.setFriendEtc(originData.getFriend().getEtcMemo());
        response.setGiftType(originData.getGiftType().getType());
        response.setGiftContent(originData.getContent());
        response.setGiftPrice(originData.getPrice());
        response.setGiftDay(originData.getGiftDay());

        return response;
    }

    public void putGiftType(long id, GiftTypeChangeRequest request){
        Gift originData = giftRepository.findById(id).orElseThrow();

        originData.setGiftType(request.getGiftType());

        giftRepository.save(originData);
    }

    public void putGiftList(long id, GiftListChangeRequest request){
        Gift originData = giftRepository.findById(id).orElseThrow();

        originData.setContent(request.getContent());
        originData.setPrice(request.getPrice());
        originData.setGiftDay(request.getGiftDay());

        giftRepository.save(originData);
    }

    public void delGift(long id){
        giftRepository.deleteById(id);
    }
}
