package com.pym.giveandtakeapi.controller;

import com.pym.giveandtakeapi.model.friend.*;
import com.pym.giveandtakeapi.service.FriendService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/friend")
public class FriendController {
    private final FriendService friendService;

    @PostMapping("/new")
    public String setFriend(@RequestBody FriendCreateRequest request){
        friendService.setFriend(request);

        return "추가 완료";
    }

    @GetMapping("/list")
    public List<FriendItem> getFriends(){
        return friendService.getFriends();
    }

    @GetMapping("/detail/{friendId}")
    public FriendResponse getFriend(@PathVariable long friendId){
        return friendService.getFriend(friendId);
    }

    @PutMapping("/info/friend-id/{friendId}")
    public String putFriendInfo(@PathVariable long friendId, @RequestBody FriendInfoChangeRequest request){
        friendService.putFriendInfo(friendId, request);

        return "친구 정보 수정 완료";
    }

    @PutMapping("/cut-return/friend-id/{friendId}")
    public String putFriendCutCancel(@PathVariable long friendId){
        friendService.putFriendCutCancel(friendId);

        return "손절 타입 수정";
    }

    @PutMapping("/cut/friend-id/{friendId}")
    public String putFriendCut(@PathVariable long friendId, @RequestBody FriendCutChangeRequest request){
        friendService.putFriendCut(friendId, request);

        return "손절 완료";
    }

}
