package com.pym.giveandtakeapi.controller;

import com.pym.giveandtakeapi.model.member.MemberDupCheckResponse;
import com.pym.giveandtakeapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/member")
public class MemberController {
    //@Autowired 사용 하면 안되는 이유:
    //1.불변
    //- 생성자 주입은 객체를 생성할 시 딱 한번만 호출되므로, 이후 의존관계를 변경할 일이 없으며 변경의 여지를 만들지 않음.
    //2.누락
    //- 순수 자바 테스트에서 의존 관계를 누락한 경우, 생성자 주입은 컴파일 오류가 발생. setter 주입의 경우 런타임 시 에러. 필드 주입의 경우에는 아예 불가능(외부 라이브러리에 테스트를 의존해야 함)
    //3. final 키워드 사용 유무
    //-생성자 주입의 경우 final 키워드 사용 가능. 그래서, 혹시라도 값이 설정되지 않는 경우 즉각적으로 컴파일 오류를 뱉어냄.
    private final MemberService memberService;

    @GetMapping("/id/duplicate/check")
    public MemberDupCheckResponse getMemberDupCheck(){

    }



}
