package com.pym.giveandtakeapi.controller;


import com.pym.giveandtakeapi.entity.Friend;
import com.pym.giveandtakeapi.model.gift.*;
import com.pym.giveandtakeapi.service.FriendService;
import com.pym.giveandtakeapi.service.GiftService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/gift")
public class GiftController {
    private final FriendService friendService;
    private final GiftService giftService;

    @PostMapping("/add/friend-id/{friendId}")
    public String setGift(@PathVariable long friendId, @RequestBody GiftCreateRequest request){
        Friend friend = friendService.getData(friendId);
        giftService.setGift(friend, request);

        return "선물 기록";
    }

    @GetMapping("/list")
    public List<GiftItem> getGifts(){
        return giftService.getGifts();
    }

    @GetMapping("/detail/{id}")
    public GiftResponse getGift(@PathVariable long id){
        return giftService.getGift(id);
    }

    @PutMapping("/type/gift-id/{giftId}")
    public String putGiftType(@PathVariable long giftId, @RequestBody GiftTypeChangeRequest request){
        giftService.putGiftType(giftId, request);

        return "수정완료";
    }

    @PutMapping("/list/gift-id/{giftId}")
    public String putGiftList(@PathVariable long giftId, @RequestBody GiftListChangeRequest request){
        giftService.putGiftList(giftId, request);

        return "수정완료";
    }

    @DeleteMapping("/gift-id/{giftId}")
    public String delGift(@PathVariable long giftId){
        giftService.delGift(giftId);

        return "삭제";
    }
}

