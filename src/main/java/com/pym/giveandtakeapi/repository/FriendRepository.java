package com.pym.giveandtakeapi.repository;

import com.pym.giveandtakeapi.entity.Friend;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FriendRepository extends JpaRepository <Friend, Long> {
}
