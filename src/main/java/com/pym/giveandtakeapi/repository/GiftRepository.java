package com.pym.giveandtakeapi.repository;

import com.pym.giveandtakeapi.entity.Gift;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GiftRepository extends JpaRepository<Gift, Long> {
}
