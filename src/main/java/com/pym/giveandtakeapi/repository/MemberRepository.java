package com.pym.giveandtakeapi.repository;

import com.pym.giveandtakeapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

// JpaRepository 에게 MemberRepository 기능 상속
public interface MemberRepository extends JpaRepository <Member, Long> {

    //id 중복 확인(이 아이디로 이미 가입을 했는지 확인)
    //username = unique
    //username과 일치하는 갯수를 알고 싶을 때(입력 받은 값)
    //jpa method()
    long countByUserName(String username);

}
