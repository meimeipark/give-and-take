package com.pym.giveandtakeapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberJoinRequest {
    private String username;
    private String password;
    private String passwordRe;
    private String name;
}
