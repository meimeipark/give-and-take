package com.pym.giveandtakeapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberDupCheckResponse {
    private Boolean isNew;
}
