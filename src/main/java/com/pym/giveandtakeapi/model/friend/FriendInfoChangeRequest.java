package com.pym.giveandtakeapi.model.friend;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FriendInfoChangeRequest {
    private String name;
    private String phoneNumber;
    private String etcMemo;
}
