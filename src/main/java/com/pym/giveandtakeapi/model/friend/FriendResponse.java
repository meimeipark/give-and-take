package com.pym.giveandtakeapi.model.friend;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FriendResponse {
    private Long id;
    private String name;
    private String phoneNumber;
    private String etcMemo;
    private String cutType;
    private LocalDate cutDate;
    private String cutReason;
}
