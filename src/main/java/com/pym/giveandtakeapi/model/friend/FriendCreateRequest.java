package com.pym.giveandtakeapi.model.friend;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FriendCreateRequest {
    private String name;
    private String phoneNumber;
    private String etcMemo;
}
