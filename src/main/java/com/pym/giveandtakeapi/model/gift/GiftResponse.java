package com.pym.giveandtakeapi.model.gift;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class GiftResponse {
    private Long friendId;
    private String friendName;
    private String friendPhoneNumber;
    private String friendEtc;
    private String giftType;
    private String giftContent;
    private Double giftPrice;
    private LocalDate giftDay;
}
