package com.pym.giveandtakeapi.model.gift;

import com.pym.giveandtakeapi.enums.GiftType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GiftTypeChangeRequest {
    @Enumerated(value = EnumType.STRING)
    private GiftType giftType;
}
