package com.pym.giveandtakeapi.model.gift;

import com.pym.giveandtakeapi.entity.Friend;
import com.pym.giveandtakeapi.enums.GiftType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GiftItem {
    private Long id;
    private String friendName;
    private String giftType;
    private String Content;
}
