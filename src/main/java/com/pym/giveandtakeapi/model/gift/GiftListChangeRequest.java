package com.pym.giveandtakeapi.model.gift;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class GiftListChangeRequest {
    private String Content;
    private Double price;
    private LocalDate giftDay;
}
