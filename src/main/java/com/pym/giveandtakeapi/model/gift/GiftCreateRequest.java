package com.pym.giveandtakeapi.model.gift;

import com.pym.giveandtakeapi.entity.Friend;
import com.pym.giveandtakeapi.enums.GiftType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class GiftCreateRequest {
    @Enumerated(value = EnumType.STRING)
    private GiftType giftType;
    private String Content;
    private Double price;
    private LocalDate giftDay;
}
