package com.pym.giveandtakeapi.entity;

import com.pym.giveandtakeapi.enums.GiftType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Gift {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "friendId", nullable = false)
    private Friend friend;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private GiftType giftType;

    @Column(nullable = false, length = 50)
    private String Content;

    @Column(nullable = false)
    //금액 산정 값은 null값이 없게 설계
    private Double price;

    @Column(nullable = false)
    private LocalDate giftDay;
}
