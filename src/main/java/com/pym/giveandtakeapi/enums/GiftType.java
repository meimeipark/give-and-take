package com.pym.giveandtakeapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor

public enum GiftType {
    GIVE("선물 줌"),
    TAKE("선물 받음");

    private final String type;
}
